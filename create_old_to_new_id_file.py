__author__ = 'justingibbons'
import csv

def create_old_to_new_dic(infile):
    out_dic=dict()
    with open(infile,"r") as csv_in:
        csv_reader=csv.reader(csv_in)
        for row in csv_reader:
            value=row[0]
            for key in row[1:]:
                if key=="":
                    continue
                else:
                    out_dic[key]=value
    return out_dic

def create_old_to_new_id_file(infile,outfile):
    old_to_new_dic=create_old_to_new_dic(infile)
    with open(outfile,"w") as out:
        csv_writer=csv.writer(out)
        csv_writer.writerow(["old_id","new_id"])
        for key in old_to_new_dic:
            csv_writer.writerow([key,old_to_new_dic[key]])
if __name__=="__main__":
    import unittest
    infile="test_files/new_to_old_id_mappings.csv"
    outfile="test_files/Outfiles/result_new_to_old_id_mappings.csv"
    create_old_to_new_id_file(infile,outfile)
    class TestCreatOldToNewIDFile(unittest.TestCase):
        def test_create_old_to_new_id_file(self):
            correct_answer={"PF13_0083":"PF3D7_1314600",
                           "PF02_0090":"PF3D7_0209400",
                            "PFB0423c":"PF3D7_0209400",
                            "PFB0425c":"PF3D7_0209400",
                           "PF11_0434":"PF3D7_1142200"}


            self.assertEqual(create_old_to_new_dic(infile),correct_answer)

    unittest.main()