__author__ = 'justingibbons'
from csv_to_json import csv_to_json
class Parse_PlasmoDB_csv:
    def __init__(self,csv_file,sep="\t"):
       self.json=csv_to_json(csv_file,delimiter=sep)
       self.gene_id='[Gene ID]'
       self.computed_go_component_id_key="[Computed GO Component IDs]"
       self.computed_go_function_id_key="[Computed GO Function IDs]"
       self.computed_go_process_id_key="[Computed GO Process IDs]"
       self.curated_go_component_id_key="[Curated GO Component IDs]"
       self.curated_go_function_id_key="[Curated GO Function IDs]"
       self.curated_go_process_id_key="[Curated GO Process IDs]"

    def organize_keys_by_gene_id(self,list_keys):
        """Takes in a list of column header values and returns a dictionary with the gene id as the key pointing to
        a dictionary were the keys are the listed column headers and the values are the values stored under that header
        for the gene"""
        return_dic=dict()
        for row in self.json:
            gene_id=row[self.gene_id]
            return_dic[gene_id]=dict()
            for key in list_keys:
                return_dic[gene_id][key]=row[key]
        return return_dic

    def get_all_go_ids(self):
        """Returns a dictionary where the key is the gene id and the value is a set of all the GO ids assigned to that
        gene. Removes N/A"""
        go_keys=[self.computed_go_component_id_key, self.computed_go_function_id_key, self.computed_go_process_id_key,
                 self.curated_go_component_id_key,self.curated_go_function_id_key, self.curated_go_process_id_key]
        return_set_dic=dict()
        no_id="N/A"
        #Get the go value for each gene
        go_string_dic=self.organize_keys_by_gene_id(go_keys)
        for gene_id in go_string_dic:
            return_set_dic[gene_id]=set()
            gene_set=return_set_dic[gene_id]
            for go_type in go_string_dic[gene_id]:
                go_list=go_string_dic[gene_id][go_type].split(",")
                for go_id in go_list:
                    go_string=go_id.strip()
                    if go_string != no_id:
                        gene_set.add(go_string)
        return return_set_dic








if __name__=="__main__":
    import unittest
    go_example_file="test_files/plasmodb_go_csv.txt"
    example_go_json=Parse_PlasmoDB_csv(go_example_file)
    class Test_Parse_PlasmoDB_csv(unittest.TestCase):

        def test_organize_keys_by_gene_id(self):
            list_of_keys=["[Computed GO Component IDs]","[Computed GO Function IDs]"]
            correct_answer={"PF3D7_0100200":{"[Computed GO Component IDs]":"N/A","[Computed GO Function IDs]":"N/A"},
                            "PF3D7_0102600":{"[Computed GO Component IDs]":"N/A","[Computed GO Function IDs]":"GO:0005524, GO:0004672"},
                            "PF3D7_0518100":{"[Computed GO Component IDs]":"N/A","[Computed GO Function IDs]":"N/A"},
                            "PF3D7_0532100":{"[Computed GO Component IDs]":"N/A","[Computed GO Function IDs]":"N/A"},
                            "PF3D7_0727800":{"[Computed GO Component IDs]":"N/A","[Computed GO Function IDs]":"GO:0046872, GO:0000166"},
                            "PF3D7_1014600":{"[Computed GO Component IDs]":"GO:0016020",
                                             "[Computed GO Function IDs]":"GO:0003677, GO:0003682, GO:0008270"}}

            self.assertEqual(example_go_json.organize_keys_by_gene_id(list_of_keys),correct_answer)

        def test_get_all_go_ids(self):
            correct_answer={"PF3D7_0100200":{"GO:0020036", "GO:0020002", "GO:0020003","GO:0020033", "GO:0020035", "GO:0020013"},
                            "PF3D7_0102600":{"GO:0005524", "GO:0004672","GO:0006468"},
                            "PF3D7_0518100":set(),
                            "PF3D7_0532100":{"GO:0020005"},
                            "PF3D7_0727800":{"GO:0046872", "GO:0000166","GO:0020011", "GO:0016021"},
                            "PF3D7_1014600":{"GO:0016020","GO:0003677","GO:0003682", "GO:0008270","GO:0016573", "GO:0006357","GO:0005515","GO:0003700"}}
            self.maxDiff=None
            self.assertEqual(example_go_json.get_all_go_ids(),correct_answer)

    unittest.main()
