__author__ = 'justingibbons'
from csv_to_json import csv_to_json
def remove_commas_from_number_string(number_string):
    return "".join(number_string.split(","))


def parse_genomic_location_string(genomic_location_string):
    """Takes in a string from EupathDb containing the genomic location information and parses out the information
    and stores and returns a dictionary with the different components stored under keys. Note this does not change
    the location information into integers. Everything remains a string"""
    #The keys (and information) stored in the dictionary
    organism_key="organism"
    chromosome_key="chromosome"
    version_key="version"
    start_key="start"
    end_key="end"
    strand_key="strand"

    organism_zome_version_string,coordinates_string=genomic_location_string.split(":")
    organism, chromosome, version=organism_zome_version_string.split("_")

    #dash isn't used for anything its just a something in the string in the way
    start, dash, end,strand=coordinates_string.strip().split(" ")

    return_dict={organism_key:organism,
                 chromosome_key:chromosome,
                 version_key:version,
                 start_key:start,
                 end_key:end,
                 strand_key:strand}
    return return_dict


def create_segment_id(genomic_location_dic):
    """Takes a dictionary like the one constructed by parse_genomic_location_string and uses it to create a
    Segment ID that can be used at EupathDB"""
    #The keys (and information) stored in the dictionary
    organism_key="organism"
    chromosome_key="chromosome"
    version_key="version"
    start_key="start"
    end_key="end"
    strand_key="strand"

    #Assemble the different pieces
    organism_zome_version_string="_".join([genomic_location_dic[organism_key],
                                        genomic_location_dic[chromosome_key],
                                        genomic_location_dic[version_key]])

    start_string=remove_commas_from_number_string(genomic_location_dic[start_key])
    end_string=remove_commas_from_number_string(genomic_location_dic[end_key])

    #Find out if its on the forward or reverse strand
    if genomic_location_dic[strand_key].find("-") != -1:
        strand_str="r"
    else:
        strand_str="f"

    return_string=organism_zome_version_string+":"+start_string+"-"+end_string+":"+strand_str

    return return_string

def get_upstream_region(genomic_location_dic,bp=2000):
    """Takes a genomic location dictionary and returns a new genomic location dictionary for the region bp upstream
     of the start site (i.e. new_start_site=old_start_site-bp and new_end_site=old_start_site-1"""
    start_key="start"
    end_key="end"
    return_genomic_location_dic=genomic_location_dic.copy() #Createa a shallow copy of a dictionary to avoid modifying the input dictionary

    #Create a copy
    old_start_site=int(remove_commas_from_number_string(genomic_location_dic[start_key]))
    new_start_site=old_start_site-bp
    new_end_site=old_start_site-1

    return_genomic_location_dic[start_key]=str(new_start_site)
    return_genomic_location_dic[end_key]=str(new_end_site)

    return return_genomic_location_dic

def get_genomic_locations_from_csv(infile,key="[Genomic Location]",delimiter=","):
    """Returns a list of genomic location strings stored in a csv under key"""
    json_obj=csv_to_json(infile,delimiter=delimiter)
    return json_obj.get_column(key=key)

def write_out_upstream_segment_ids_to_file(infile,outfile,key="[Genomic Location]",delimiter=",",bp=2000):
    """Takes a csv with a genomic location string stored under key and writes out new genomic segment ids for the
    region upstream region identified by bp. Writes the result to outfile and also returns the segment ids as a list"""

    #Read in the data
    genomic_location_strings_list=get_genomic_locations_from_csv(infile,key,delimiter)

    #Split the data into its components so it can be manipulated
    genomic_location_dic_list=[]
    for loc_string in genomic_location_strings_list:
        genomic_location_dic_list.append(parse_genomic_location_string(loc_string))

    #Create new genomic_location dictionaries with the new locations
        upstream_genomic_location_dic_list=[]
        for loc_dic in genomic_location_dic_list:
            upstream_genomic_location_dic_list.append(get_upstream_region(loc_dic,bp=bp))

    #Create the segment ids
    segment_ids_list=[]
    for loc_dic in upstream_genomic_location_dic_list:
        segment_ids_list.append(create_segment_id(loc_dic))

    #Get the segment ids and write them to outfile
    with open(outfile,"w") as out:
        out.write("\n".join(segment_ids_list))

    return segment_ids_list







if __name__=="__main__":
    import unittest

    class TestGenomicLocationTools(unittest.TestCase):
        def setUp(self):

            self.organism_key="organism"
            self.chromosome_key="chromosome"
            self.version_key="version"
            self.start_key="start"
            self.end_key="end"
            self.strand_key="strand"

            self.genomic_location1="Pf3D7_02_v3: 308,847 - 312,155 (-)"
            self.genomic_location1_segment_id="Pf3D7_02_v3:308847-312155:r"
            self.genomic_location2="Pf3D7_04_v3: 1,076,347 - 1,077,534 (+)"
            self.genomic_location2_segment_id="Pf3D7_04_v3:1076347-1077534:f"
            self.genomic_location1_dict={self.organism_key:"Pf3D7",
                            self.chromosome_key:"02",
                            self.version_key:"v3",
                            self.start_key:"308,847",
                            self.end_key:"312,155",
                            self.strand_key:"(-)"}
            self.genomic_location1_new_start_site_dict={self.organism_key:"Pf3D7",
                            self.chromosome_key:"02",
                            self.version_key:"v3",
                            self.start_key:"306847",
                            self.end_key:"308846",
                            self.strand_key:"(-)"}

            self.genomic_location1_new_start_segment_id="Pf3D7_02_v3:306847-308846:r"
            self.genomic_location2_dict={self.organism_key:"Pf3D7",
                            self.chromosome_key:"04",
                            self.version_key:"v3",
                            self.start_key:"1,076,347",
                            self.end_key:"1,077,534",
                            self.strand_key:"(+)"}
            self.genomic_location_2_new_start_site_dict={self.organism_key:"Pf3D7",
                            self.chromosome_key:"04",
                            self.version_key:"v3",
                            self.start_key:"1074347",
                            self.end_key:"1076346",
                            self.strand_key:"(+)"}
            self.genomic_location_2_new_start_site_segment_id="Pf3D7_04_v3:1074347-1076346:f"

            self.eupathdb_csv="test_files/plasmodb_csv1.csv"
            self.write_out_upstream_segment_ids_to_file_outfile="test_files/Outfiles/write_out_upstream_segment_ids_to_file_outfile.txt"


        def test_parse_genomic_location_string(self):

            self.assertEqual(parse_genomic_location_string(self.genomic_location1),self.genomic_location1_dict)

        def test_create_segment_id(self):
            self.assertEqual(create_segment_id(self.genomic_location1_dict),self.genomic_location1_segment_id)
            self.assertEqual(create_segment_id(self.genomic_location2_dict),self.genomic_location2_segment_id)

        def test_get_upstream_region_copy_working(self):
            original_genomic_location1={self.organism_key:"Pf3D7",
                            self.chromosome_key:"02",
                            self.version_key:"v3",
                            self.start_key:"308,847",
                            self.end_key:"312,155",
                            self.strand_key:"(-)"}
            result=get_upstream_region(self.genomic_location1_dict)
            self.assertEqual(result,self.genomic_location1_new_start_site_dict)
            self.assertEqual(self.genomic_location1_dict,original_genomic_location1)

        def test_get_upstream_region_multiple_commas(self):
            result=get_upstream_region(self.genomic_location2_dict)
            self.assertEqual(result,self.genomic_location_2_new_start_site_dict)

        def test_get_genomic_locations_from_csv(self):
            correct=[self.genomic_location1,self.genomic_location2]
            self.assertEqual(get_genomic_locations_from_csv(self.eupathdb_csv),correct)

        def test_write_out_upstream_segment_ids_to_file(self):
            correct=[self.genomic_location1_new_start_segment_id,self.genomic_location_2_new_start_site_segment_id]
            self.assertEqual(write_out_upstream_segment_ids_to_file(self.eupathdb_csv,self.write_out_upstream_segment_ids_to_file_outfile),
                             correct)


    unittest.main()

