__author__ = 'justingibbons'


def get_set_from_string(string,sep=",",strip_white_space=True):
    """Takes a string and returns a set that is created by seperating based on sep"""

    object_list=string.split(sep)
    if strip_white_space:
        return_set=set()
        for object in object_list:
            return_set.add(object.strip())
    else:
        return_set=set(object_list)
    return return_set

class Parse_PlasmoDB_Json:
    def __init__(self,json_object):
        """Create a dictionary where the keys are the "id" field and the values are the rest
        of the data"""
        self.dictionary=dict()
        self.response_key="response"
        self.recordset_key="recordset"
        self.records_key="records"
        self.id_key="id"
        self.fields_key="fields"
        self.name_key="name"
        self.rows_key="rows"
        self.value_key="value"
        self.tables_key="tables"
        for record in json_object[self.response_key][self.recordset_key][self.records_key]:
            key=record[self.id_key]
            field=record[self.fields_key]
            tables=record[self.tables_key]
            self.dictionary[key]={self.fields_key:field,
                                  self.tables_key:tables}

    def __str__(self):
        return str(self.dictionary)

    def get_value(self,value):
        """Takes a value string returns a dictionary where the gene ids are keys and the values cooresponding
         to the value string are the values. This won't work if the data is in tables form. If the data is in
         tables form use the method get_table_rows."""

        return_dic=dict()

        for gene in self.dictionary:
            for fields in self.dictionary[gene][self.fields_key]:
                if fields[self.name_key]==value:
                    return_dic[gene]=fields[self.value_key]
                else:
                    continue
        return return_dic

    def get_table_rows(self,table_name):
        """Returns a dictionary where the gene id is the key and the value is a list containing the row information"""
        return_dic=dict()
        for gene in self.dictionary:
            for tables in self.dictionary[gene][self.tables_key]:
                if tables[self.name_key]==table_name:
                    return_dic[gene]=tables[self.rows_key]
                else:
                    continue
        return return_dic

    def get_y2h_interactors(self):
        """Returns a dictionary where the key is a gene accession numbers and the value is a list of all the
        other genes that have been shown to interact with that gene via a yeast 2 hybrid experiment"""
        y2h_key="Y2H Interactions"
        interactor_key="other_source_id"
        row_index=0 #The method get_table_rows only returns a single table
        return_dic=dict()
        y2h_table_data=self.get_table_rows(y2h_key)
        for gene in y2h_table_data:
            return_dic[gene]=[]
            rows=y2h_table_data[gene]
            if len(rows) > 0: #If there's data get it
                for row in rows:
                    for data in row[self.fields_key]:
                        if data[self.name_key]==interactor_key:
                            return_dic[gene].append(data[self.value_key])
                        else:
                            continue
            else:
                continue
        return return_dic

    def get_unique_y2h_interactions(self):
        """Returns a list of tuples containing unique Y2H interactions so that if there is a tuple (node1, node2)
        there is not tuple (node2,node2)"""

        unique_y2h_list=[]
        unique_pairs_set=set()
        y2h_dic=self.get_y2h_interactors()

        #Filter out redundant pairings
        for gene in y2h_dic:
            for interactor in y2h_dic[gene]:
                unique_pairs_set.add(frozenset([gene,interactor]))

        #Create the tuple list. Tuples are needed so that they can be unpacked latter
        for pair in unique_pairs_set:
            unique_y2h_list.append(tuple(pair))

        return unique_y2h_list

    def get_y2h_interaction_strength(self):
        """Creates a dictionary where the keys are frozen sets of interacting genes (plasmodb_ids) and the values are
        times_reproduced."""
        pass

    def old_id_to_new_dict(self):
        """Create a dictionary that maps between old IDs and new IDs. Consistent capitalization is not used when
        reporting plasmodb accession numbers so all accessions used as keys in this dictionary will be normalized to all
        capital letters."""
        old_ids_key="old_ids"
        self.old_id_to_new_dict=dict()
        old_ids=self.get_value(old_ids_key)
        for key in old_ids:
            for old_id in old_ids[key].split(","): #The value is a comma seperated string
                self.old_id_to_new_dict[old_id.strip().upper()]=key  #Strip to remove white space from the id

        return self.old_id_to_new_dict

    def get_go_category_id_sets(self,go_category,annotated_go=True,predicted_go=True):
        """Returns a dictionary where the values are a set the GO category ids for each gene. If annotated_go=True the annotated
         GO ids are included and if predicted_go=True predicited Go ids are included. go_cat maybe either process,
         function or component."""
        annotated_go__process_key="ann_go_id_"+go_category.lower()
        predicted_go_process_key="pred_go_id_"+go_category.lower()

        if annotated_go:
            annotated_go_id_set_dic=dict()
            annotated_go_dict=self.get_value(annotated_go__process_key)
            for key in annotated_go_dict:
                go_string=annotated_go_dict[key]
                annotated_go_id_set_dic[key]=get_set_from_string(go_string)
                if "" in annotated_go_id_set_dic[key]:
                    annotated_go_id_set_dic[key].remove("") #Remove a blank string

        if predicted_go:
            predicted_go_id_set_dic=dict()
            predicted_go_dict=self.get_value(predicted_go_process_key)
            for key in predicted_go_dict:
                go_string=predicted_go_dict[key]
                predicted_go_id_set_dic[key]=get_set_from_string(go_string)
                if "" in predicted_go_id_set_dic[key]:
                    predicted_go_id_set_dic[key].remove("")

        if annotated_go==True and predicted_go==False:
            return annotated_go_id_set_dic

        if annotated_go==False and predicted_go==True:
            return predicted_go_id_set_dic

        if annotated_go==True and predicted_go==True:
            union_dict=dict()
            for key in annotated_go_id_set_dic:
                union_dict[key]=annotated_go_id_set_dic[key].union(predicted_go_id_set_dic[key])
            return union_dict

    def get_group_go_similarity(self,groups,go_category,annotated_go=True,predicted_go=True):
        """Takes a dictionary where the key is a group id and the value is an iterable of gene accession numbers
        belonging to that group. Returns a dictionary of containing the union and intersection of all of the go terms
        for the group. go_category must be one of: process, function, component"""

        #Create the return object

        group_go_sets=dict()
        union_key="union"
        intersection_key="intersection"

        #Retrive the GO categories for all genes
        gene_go_sets=self.get_go_category_id_sets(go_category,annotated_go=annotated_go,predicted_go=predicted_go)

        for group_id in groups:
            set_list=[]
            group_go_sets[group_id]={union_key:set(),
                                     intersection_key:set()}
            for gene_id in groups[group_id]:
                set_list.append(gene_go_sets[gene_id])
            group_go_sets[group_id][union_key]=set.union(*set_list)
            group_go_sets[group_id][intersection_key]=set.intersection(*set_list)
        return group_go_sets






    def get_ids_by_go_ids(self,go_category,go_ids,annotated_go=True,predicted_go=True,outfile=None):
        """Takes an iterable of go_ids and returns a list of accession numbers corresponding to genes that have
        at least one go id annotation in go_ids. Can limit search to annotated, predicted or both.
        If outfile is supplied the result is written out.
        go_category must be one of: process, function, component"""

        accs_with_go_ids=[]
        go_ids_set=set(go_ids)

        go_ids_set_dict=self.get_go_category_id_sets(go_category=go_category,annotated_go=annotated_go,predicted_go=predicted_go)

        for id in go_ids_set_dict:
            if len(go_ids_set & go_ids_set_dict[id])>0:
                accs_with_go_ids.append(id)

        if outfile != None:
            with open(outfile,"w") as out:
                out.write("\n".join(accs_with_go_ids))
        return accs_with_go_ids

    def get_organism_and_ids(self):
        """Returns a dictionary where the key is a gene accession and the value is a dictionary containing the organism,
        Uniprot ID, Entrez Gene ID, and Gene name or symbol"""
        organism_key="organism"
        uniprot_id_key="uniprot_id"
        entrez_id_key="entrez_id"
        gene_name_key="gene_name"

        return_dic=dict()
        values_to_get_list=[organism_key,uniprot_id_key,entrez_id_key,gene_name_key]

        for key in values_to_get_list:
            values_dic=self.get_value(key)
            for gene in values_dic:
                if gene in return_dic:
                    return_dic[gene][key]=values_dic[gene]
                else:
                    return_dic[gene]={key:values_dic[gene]}
        return return_dic

    def get_population_biology(self):
        """Returns a dictionary where the gene accesion is the primary key and the value is a dictionary containing
        the population biology information"""
        total_snps_key="total_hts_snps"
        nonsyn_snps_key="hts_nonsynonymous_snps"
        syn_snps_key="hts_synonymous_snps"
        noncoding_snps_key="hts_noncoding_snps"
        snps_with_stop_codons_key="hts_stop_codon_snps"
        nonsyn_syn_snp_ratio_key="hts_nonsyn_syn_ratio"

        return_dic=dict()
        values_to_get_list=[total_snps_key,nonsyn_snps_key,syn_snps_key,
                            noncoding_snps_key,snps_with_stop_codons_key,nonsyn_syn_snp_ratio_key]
        for key in values_to_get_list:
            values_dic=self.get_value(key)
            for gene in values_dic:
                if gene in return_dic:
                    return_dic[gene][key]=values_dic[gene]
                else:
                    return_dic[gene]={key:values_dic[gene]}
        return return_dic


if __name__=="__main__":
    import unittest
    from json_utils import open_json_object


    infile="test_files/GeneByLocusTag_detail.txt"

    pd_json=open_json_object(infile)

    pd_obj=Parse_PlasmoDB_Json(pd_json)

    class Test_Parse_PlasmoDb(unittest.TestCase):
        def test_get_value(self):
            correct_answer={"PF3D7_0100200":"RIF",
                            "PF3D7_0102600":"FIKK1",
                            "PF3D7_0518100":"",
                            "PF3D7_0532100":"ETRAMP5",
                            "PF3D7_0727800":"",
                            "PF3D7_1014600":"ADA2"}
            self.assertEquals(pd_obj.get_value("gene_name"),correct_answer)


        def test_get_table_rows(self):
            correct_answer={"PF3D7_0100200":[],
            "PF3D7_0102600":[{"fields":[{"name":"ec_number","value":"2.7.11.1"},
                                        {"name":"source","value":"MPMP"},
                                        {"name":"ec_description","value":"Non-specific serine/threonine protein kinase"}]},
                             {"fields":[{"name":"ec_number","value":"2.7.11.1"},{"name":"source","value":"GeneDB"},{"name":"ec_description","value":"Non-specific serine/threonine protein kinase"}]},
                             {"fields":[{"name":"ec_number","value":"2.7.11.1"},{"name":"source","value":"inferred from OrthoMCL"},
                                        {"name":"ec_description","value":"Non-specific serine/threonine protein kinase"}]}],
            "PF3D7_0518100":[],
            "PF3D7_0532100":[],
            "PF3D7_0727800":[{"fields":[{"name":"ec_number","value":"3.6.3.12"},{"name":"source","value":"MPMP"},{"name":"ec_description","value":"Potassium-transporting ATPase"}]},
                             {"fields":[{"name":"ec_number","value":"3.6.3.12"},{"name":"source","value":"GeneDB"},
                                        {"name":"ec_description","value":"Potassium-transporting ATPase"}]},
                             {"fields":[{"name":"ec_number","value":"3.6.3.-"},{"name":"source","value":"inferred from OrthoMCL"},
                                        {"name":"ec_description","value":"Acting on acid anhydrides; catalyzing transmembrane movement of substances."}]}],
            "PF3D7_1014600": []}
            self.assertEqual(pd_obj.get_table_rows("EC Number"),correct_answer)

        def test_get_y2h_interactors(self):
            correct_answer={"PF3D7_0100200":[],
                            "PF3D7_0102600":["PF3D7_0532100","PF3D7_0727800","PF3D7_1014600","PF3D7_0518100"],
                            "PF3D7_0518100":["PF3D7_0303300","PF3D7_0102600","PF3D7_0624600"],
                            "PF3D7_0532100":["PF3D7_1205600","PF3D7_0617200","PF3D7_0802100","PF3D7_1239200",
                                             "PF3D7_0811300","PF3D7_1113400","PF3D7_0309300","PF3D7_0624600",
                                             "PF3D7_1308200","PF3D7_0418300","PF3D7_1339700","PF3D7_0524600",
                                             "PF3D7_1466800","PF3D7_0704600","PF3D7_1316600","PF3D7_0710700",
                                             "PF3D7_1023900","PF3D7_1129100","PF3D7_0301600","PF3D7_1452400",
                                             "PF3D7_0424000","PF3D7_1473200","PF3D7_0102600","PF3D7_0526700",
                                             "PF3D7_1455700","PF3D7_1362400","PF3D7_0108300","PF3D7_1149000",
                                             "PF3D7_0511500"],
                            "PF3D7_0727800":["PF3D7_0102600"],
                            "PF3D7_1014600":["PF3D7_1417800","PF3D7_0405400","PF3D7_1358600","PF3D7_0527500","PF3D7_0323700",
                                             "PF3D7_1340900","PF3D7_0102600","PF3D7_1116700","PF3D7_1238500","PF3D7_0916900",
                                             "PF3D7_0716000","PF3D7_0500800","PF3D7_0214100","PF3D7_0721000"]

                            }
            self.assertEqual(pd_obj.get_y2h_interactors(),correct_answer)

        def test_get_unique_y2h_interactions(self):
            two_gene_json="test_files/2_gene_plasmodb_json.txt"
            two_gene_json=open_json_object(two_gene_json)
            two_gene_obj=Parse_PlasmoDB_Json(two_gene_json)
            correct_answer=set([frozenset(["PF3D7_0518100","PF3D7_0303300"]),frozenset(["PF3D7_0518100","PF3D7_0102600"]),
                            frozenset(["PF3D7_0518100","PF3D7_0624600"]),frozenset(["PF3D7_0727800","PF3D7_0102600"])])
            response_set=set()

            response=two_gene_obj.get_unique_y2h_interactions()

            #Have to create a set of frozen sets to check answer because the ordering may not be stable
            for pair in response:
                response_set.add(frozenset(pair))

            self.assertEqual(response_set,correct_answer)


        def test_get_organism_and_ids(self):
            correct_answer={"PF3D7_0100200":{"organism":"P. falciparum 3D7","uniprot_id":"","entrez_id":"","gene_name":"RIF"},
                            "PF3D7_0102600":{"organism":"P. falciparum 3D7","uniprot_id":"","entrez_id":"","gene_name":"FIKK1"},
                            "PF3D7_0518100":{"organism":"P. falciparum 3D7","uniprot_id":"Q8I3S7","entrez_id":"812996","gene_name":""},
                            "PF3D7_0532100":{"organism":"P. falciparum 3D7","uniprot_id":"Q8I3F3","entrez_id":"813127","gene_name":"ETRAMP5"},

                            "PF3D7_0727800":{"organism":"P. falciparum 3D7","uniprot_id":"Q8IBH9","entrez_id":"2655168","gene_name":""},
                            "PF3D7_1014600":{"organism":"P. falciparum 3D7","uniprot_id":"Q8IJP9","entrez_id":"810301","gene_name":"ADA2"}}
            self.assertEqual(pd_obj.get_organism_and_ids(),correct_answer)

        def test_get_population_biology(self):
            correct_answer={"PF3D7_0100200":{"total_hts_snps":"510","hts_nonsynonymous_snps":"396","hts_synonymous_snps":"83","hts_noncoding_snps":"22","hts_stop_codon_snps":"9","hts_nonsyn_syn_ratio":"4.77"},
                            "PF3D7_0102600":{"total_hts_snps":"136","hts_nonsynonymous_snps":"73","hts_synonymous_snps":"29","hts_noncoding_snps":"33","hts_stop_codon_snps":"1","hts_nonsyn_syn_ratio":"2.52"},
                            "PF3D7_0518100":{"total_hts_snps":"257","hts_nonsynonymous_snps":"160","hts_synonymous_snps":"72","hts_noncoding_snps":"17","hts_stop_codon_snps":"8","hts_nonsyn_syn_ratio":"2.22"},
                            "PF3D7_0532100":{"total_hts_snps":"40","hts_nonsynonymous_snps":"24","hts_synonymous_snps":"16","hts_noncoding_snps":"0","hts_stop_codon_snps":"0","hts_nonsyn_syn_ratio":"1.5"},
                            "PF3D7_0727800":{"total_hts_snps":"429","hts_nonsynonymous_snps":"286","hts_synonymous_snps":"138","hts_noncoding_snps":"0","hts_stop_codon_snps":"5","hts_nonsyn_syn_ratio":"2.07"},
                            "PF3D7_1014600":{"total_hts_snps":"357","hts_nonsynonymous_snps":"236","hts_synonymous_snps":"119","hts_noncoding_snps":"0","hts_stop_codon_snps":"2","hts_nonsyn_syn_ratio":"1.98"}}

            self.assertEqual(pd_obj.get_population_biology(),correct_answer)


        def test_old_new_ids_dict(self):
            correct_answer={"MAL1P4.02":"PF3D7_0100200",
                            "PFA0010C":"PF3D7_0100200",
                            "MAL1P1.17":"PF3D7_0102600",
                            "PFA0130C":"PF3D7_0102600",
                            "MAL5P1.181":"PF3D7_0518100",
                           "PFE0905W":"PF3D7_0518100",
                            "MAL5P1.312":"PF3D7_0532100",
                            "PFE1590W":"PF3D7_0532100",
                            "PF07_0115":"PF3D7_0727800",
                            "PF10_0143":"PF3D7_1014600"}

            self.assertEqual(pd_obj.old_id_to_new_dict(),correct_answer)

        def test_get_go_category_id_sets_ann(self):
            #Test using process
            correct_answer={"PF3D7_0100200":{"GO:0020033","GO:0020035","GO:0020013"},
                            "PF3D7_0102600":{"GO:0006468"},
                            "PF3D7_0518100":set(),
                            "PF3D7_0532100":set(),
                            "PF3D7_0727800":{"GO:0006812"},
                            "PF3D7_1014600":{"GO:0016573", "GO:0006357", "GO:0006355"}}

            self.assertEqual(pd_obj.get_go_category_id_sets(annotated_go=True,predicted_go=False,go_category="process"),correct_answer)

        def test_get_go_category_id_sets_pred(self):
            #Test using process
            correct_answer={"PF3D7_0100200":set(),
                            "PF3D7_0102600":{"GO:0006468"},
                            "PF3D7_0518100":set(),
                            "PF3D7_0532100":set(),
                            "PF3D7_0727800":set(),
                            "PF3D7_1014600":set()}

            self.assertEqual(pd_obj.get_go_category_id_sets(annotated_go=False,predicted_go=True,go_category="process"),correct_answer)

        def test_get_go_category_id_sets_union(self):
            """Test to see if return correct results if want annotated and predicted GO ids.
            Tested using go_category="function"""
            correct_answer={"PF3D7_0100200":set(),
                            "PF3D7_0102600":{"GO:0005524", "GO:0004672"},
                            "PF3D7_0518100":set(),
                            "PF3D7_0532100":set(),
                            "PF3D7_0727800":{"GO:0046872", "GO:0000166","GO:0016887"},
                            "PF3D7_1014600":{"GO:0003677", "GO:0003682", "GO:0008270","GO:0005515", "GO:0003700"}}

            self.assertEqual(pd_obj.get_go_category_id_sets(go_category="function",predicted_go=True,annotated_go=True),correct_answer)

        def test_get_group_go_similarity(self):
            group_dict={1:{"PF3D7_0102600","PF3D7_0727800"},
                       2:{"PF3D7_1014600","PF3D7_0518100"}}

            correct_answer={1:{"union":{"GO:0005524", "GO:0004672","GO:0046872", "GO:0000166","GO:0016887"},
                        "intersection":set()},
                    2:{"union":{"GO:0003677", "GO:0003682", "GO:0008270","GO:0005515", "GO:0003700"},
                    "intersection":set()}}

            self.assertEqual(pd_obj.get_group_go_similarity(groups=group_dict,go_category="function",annotated_go=True,predicted_go=True),correct_answer)

        def test_get_ids_by_go_ids(self):
            go_ids={"GO:0005524","GO:0000166"}
            correct_answer={"PF3D7_0102600","PF3D7_0727800"}
            outfile="test_files/Outfiles/out_test_get_ids_by_go_ids.txt"
            self.assertEqual(set(pd_obj.get_ids_by_go_ids(go_category="function",go_ids=go_ids,predicted_go=True,annotated_go=True,
                                                      outfile=outfile)),correct_answer)

    unittest.main()






